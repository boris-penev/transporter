/**
 * @file
 */
#define _POSIX_C_SOURCE 200809L
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <inttypes.h>
#include <errno.h>
#include <limits.h>
#include <assert.h>
#include <math.h>

#include "main.h"
#include "init.h"

#pragma GCC diagnostic ignored "-Wunused-function"

/* if tmp_i and tmp_f are local variables, the program becomes 2% slower */
static int64_t      tmp_i;      /**< to check strtoll return values */
static float        tmp_f;      /**< to check strtof return values */
static char        *endptr;     /**< to check strtoll and strtof return values */
static char        *token = NULL;          /**< line tokenizer token */
static char        *token_saveptr = NULL;  /**< line tokenizer save pointer */
static const char * const whitespace = " \t\n"; /** line tokenizer delimiter */
static char        *filename;   /**< Name of input file. It can be either
                                 * absolute or relative to the working
                                 * directory. */
/* if line, len and read are local variables, the program becomes 2% slower */
static char        *line = NULL;  /**< for line reading */
static size_t       len = 0u;     /**< size of line buffer */
static ssize_t      read;         /**< length of current line read */
static unsigned int line_number;  /**< current line number at the open file */
static char        *line_copy = NULL;   /**< for line copying */

static void print_filename_linenumber (void);
static void save_line (void);
static float parse_ufloat (const char * const name);
static float parse_ufloat_area (const char * const name, unsigned int area_i);
static unsigned int parse_uint (const char * const name);
static unsigned int parse_uint (const char * const name);
static unsigned int parse_puint (const char * const name);
static unsigned int parse_puint_area (const char * const name,
                                      unsigned int area_i);
static unsigned int parse_uint_graph (char * const str, unsigned int area_i,
                                      unsigned int i, unsigned int j);
static void parse_disposal_rate (void);
static void parse_frequency (void);
static void parse_rest_of_line (void);
static void parse_input_file (void);
static void check_variables (void);
static void print_areas (void);
static void print_areas_2 (void);
extern int          cmp_pri (pqueue_pri_t next, pqueue_pri_t curr);
extern pqueue_pri_t get_pri (void *a);
extern void         set_pri (void *a, pqueue_pri_t pri);
extern size_t       get_pos (void *a);
extern void         set_pos (void *a, size_t pos);

/**
 * Print filename, line number and line.
 */
static void print_filename_linenumber (void)
{
  fprintf(stderr, "%s:%u: %s", filename, line_number, line_copy);
}

/**
 * Save current line in line_copy
 */
static void save_line (void)
{
  static size_t       copy_len = 0u;      /**< buffer size for line copying */
  assert(len > 0u);
  if (copy_len < len) {
    copy_len = len;
    line_copy = realloc(line_copy, len);
    assert(line_copy != NULL);
  }
  strcpy(line_copy, line);
}

/**
 * Parse positive unsigned float for global variables
 * @param name Name that will be printed in the error message
 * @return value of the parsed number
 * @note Supports hexadecimal notation
 */
static float parse_ufloat (const char * const name)
{
  token = strtok_r(NULL, whitespace, &token_saveptr);
  if (token == NULL) {
    print_filename_linenumber();
    fprintf(stderr, "premature end of line, %s expected\n", name);
    exit(EXIT_FAILURE);
  }

  errno = 0;    /* To distinguish success/failure after call */
  tmp_f = strtof(token, &endptr);

  /* Check for various possible errors */
  if (errno != 0 || *endptr != '\0' || !isfinite(tmp_f)) {
    print_filename_linenumber();
    fprintf(stderr, "%s is invalid value: '%s'\n", name, token);
    if (errno) {
      perror(NULL);
    }
    exit(EXIT_FAILURE);
  }
  if (tmp_f <= 0.0f) {
    print_filename_linenumber();
    fprintf(stderr, "%s is non-positive: %f\n", name, tmp_f);
    exit(EXIT_FAILURE);
  }

  return tmp_f;
}

/**
 * Parse positive unsigned float for area variables
 * @param name Name that will be printed in the error message
 * @param area_i index for current area
 * @return value of the parsed number
 * @note Supports hexadecimal notation
 */
static float parse_ufloat_area (const char * const name, unsigned int area_i)
{
  token = strtok_r(NULL, whitespace, &token_saveptr);
  if (token == NULL) {
    print_filename_linenumber();
    fprintf(stderr, "premature end of line, %s expected\n", name);
    exit(EXIT_FAILURE);
  }

  errno = 0;    /* To distinguish success/failure after call */
  tmp_f = strtof(token, &endptr);

  /* Check for various possible errors */
  if (errno != 0 || *endptr != '\0' || !isfinite(tmp_f)) {
    print_filename_linenumber();
    fprintf(stderr, "area %u %s is invalid value: '%s'\n", area_i, name, token);
    if (errno) {
      perror(NULL);
    }
    exit(EXIT_FAILURE);
  }
  if (tmp_f <= 0.0f) {
    print_filename_linenumber();
    fprintf(stderr, "area %u %s is non-positive: %f\n", area_i, name, tmp_f);
    exit(EXIT_FAILURE);
  }

  return tmp_f;
}

/**
 * Parse unsigned integer for global variables
 * @param name Name that will be printed in the error message
 * @return value of the parsed number
 * @note Does not support hexadecimal notation
 */
static unsigned int parse_uint (const char * const name)
{
  token = strtok_r(NULL, whitespace, &token_saveptr);
  if (token == NULL) {
    print_filename_linenumber();
    fprintf(stderr, "premature end of line, %s expected\n", name);
    exit(EXIT_FAILURE);
  }

  errno = 0;    /* To distinguish success/failure after call */
  tmp_i = strtoll(token, &endptr, 10);

  /* Check for various possible errors */
  if (errno != 0 || *endptr != '\0' || tmp_i > UINT_MAX) {
    print_filename_linenumber();
    fprintf(stderr, "%s is invalid value: '%s'\n", name, token);
    if (errno) {
      perror(NULL);
    }
    exit(EXIT_FAILURE);
  }
  if (tmp_i < 0L) {
    print_filename_linenumber();
    fprintf(stderr, "%s is negative: %" PRId64 "\n", name, tmp_i);
    exit(EXIT_FAILURE);
  }
  return (unsigned int)tmp_i;
}

/**
 * Parse positive unsigned integer for global variables
 * @param name Name that will be printed in the error message
 * @return value of the parsed number
 * @note Does not support hexadecimal notation
 */
static unsigned int parse_puint (const char * const name)
{
  token = strtok_r(NULL, whitespace, &token_saveptr);
  if (token == NULL) {
    print_filename_linenumber();
    fprintf(stderr, "premature end of line, %s expected\n", name);
    exit(EXIT_FAILURE);
  }

  errno = 0;    /* To distinguish success/failure after call */
  tmp_i = strtoll(token, &endptr, 10);

  /* Check for various possible errors */
  if (errno != 0 || *endptr != '\0' || tmp_i > UINT_MAX) {
    print_filename_linenumber();
    fprintf(stderr, "%s is invalid value: '%s'\n", name, token);
    if (errno) {
      perror(NULL);
    }
    exit(EXIT_FAILURE);
  }
  if (tmp_i <= 0L) {
    print_filename_linenumber();
    fprintf(stderr, "%s is non-positive: %" PRId64 "\n", name, tmp_i);
    exit(EXIT_FAILURE);
  }
  return (unsigned int)tmp_i;
}

/**
 * Parse positive unsigned integer for area variables
 * @param name Name that will be printed in the error message
 * @param area_i index for current area
 * @return value of the parsed number
 * @note Does not support hexadecimal notation
 */
static unsigned int parse_puint_area (const char * const name,
                                      unsigned int area_i)
{
  token = strtok_r(NULL, whitespace, &token_saveptr);
  if (token == NULL) {
    print_filename_linenumber();
    fprintf(stderr, "premature end of line, %s expected\n", name);
    exit(EXIT_FAILURE);
  }

  errno = 0;    /* To distinguish success/failure after call */
  tmp_i = strtoll(token, &endptr, 10);

  /* Check for various possible errors */
  if (errno != 0 || *endptr != '\0' || tmp_i > UINT_MAX) {
    print_filename_linenumber();
    fprintf(stderr, "area %u %s is invalid value: '%s'\n", area_i, name, token);
    if (errno) {
      perror(NULL);
    }
    exit(EXIT_FAILURE);
  }
  if (tmp_i <= 0L) {
    print_filename_linenumber();
    fprintf(stderr, "area %u %s is non-positive: %" PRId64 "\n",
            area_i, name, tmp_i);
    exit(EXIT_FAILURE);
  }
  return (unsigned int)tmp_i;
}

/**
 * Parse unsigned integer for graph variables
 *
 * @param str     strtok_r first argument, string from which to extract token
 * @param area_i  index for current area
 * @param i       index for graph rows
 * @param j       index for graph columns
 * @return value of the parsed number
 * @note Does not support hexadecimal notation
 */
static unsigned int parse_uint_graph (char * const str, unsigned int area_i,
                                      unsigned int i, unsigned int j)
{
  token = strtok_r(str, whitespace, &token_saveptr);
  if (token == NULL) {
    print_filename_linenumber();
    fprintf(stderr, "premature end of line, area %u graph[%u][%u] expected\n",
            area_i, i, j);
    exit(EXIT_FAILURE);
  }

  errno = 0;    /* To distinguish success/failure after call */
  tmp_i = strtoll(token, &endptr, 10);

  /* Check for various possible errors */
  /* Checks for USHRT_MAX because of specification but can handle UINT_MAX */
  if (errno != 0 || *endptr != '\0' || tmp_i > USHRT_MAX) {
    print_filename_linenumber();
    fprintf(stderr, "area %u graph[%u][%u] is invalid value: '%s'\n",
            area_i, i, j, token);
    if (errno) {
      perror(NULL);
    }
    exit(EXIT_FAILURE);
  }
  if (tmp_i < 0L) {
    print_filename_linenumber();
    fprintf(stderr, "area %u graph[%u][%u] is negative: %" PRId64 "\n",
            area_i, i, j, tmp_i);
    exit(EXIT_FAILURE);
  }
  return (unsigned int)tmp_i;
}

static void parse_disposal_rate (void)
{
  size_t disp_size;
  unsigned int i;
  token = strtok_r(NULL, whitespace, &token_saveptr);
  if (token == NULL) {
    print_filename_linenumber();
    fprintf(stderr, "premature end of line, disposal rate expected\n");
    exit(EXIT_FAILURE);
  }
  if (strcmp(token, "experiment") == 0) {
    disp_size = 64 * sizeof(float);
    i = 0;
    expdsprate = malloc(disp_size);
    assert(expdsprate != NULL);
    do {
      token = strtok_r(NULL, whitespace, &token_saveptr);
      if (token == NULL) {
        expdsprate[i] = 0.0f;
        break;
      }
      errno = 0;    /* To distinguish success/failure after call */
      tmp_f = strtof(token, &endptr);

      /* Check for various possible errors */
      if (errno != 0 || *endptr != '\0' || !isfinite(tmp_f)) {
        print_filename_linenumber();
        fprintf(stderr, "disposal rate is invalid value: '%s'\n", token);
        if (errno) {
          perror(NULL);
        }
        exit(EXIT_FAILURE);
      }
      if (tmp_f <= 0.0f) {
        print_filename_linenumber();
        fprintf(stderr, "disposal rate is non-positive: %f\n", tmp_f);
        exit(EXIT_FAILURE);
      }
      if (i == disp_size - 1) {
        disp_size *= 2;
        expdsprate = realloc(expdsprate, disp_size);
        assert(expdsprate != NULL);
      }
      expdsprate[i] = tmp_f;
      ++i;
    } while (1);
  } else {
    errno = 0;    /* To distinguish success/failure after call */
    tmp_f = strtof(token, &endptr);

    /* Check for various possible errors */
    if (errno != 0 || *endptr != '\0' || !isfinite(tmp_f)) {
      print_filename_linenumber();
      fprintf(stderr, "disposal rate is invalid value: '%s'\n", token);
      if (errno) {
        perror(NULL);
      }
      exit(EXIT_FAILURE);
    }
    if (tmp_f <= 0.0f) {
      print_filename_linenumber();
      fprintf(stderr, "disposal rate is non-positive: %f\n", tmp_f);
      exit(EXIT_FAILURE);
    }

    disposal_rate = tmp_f;
  }
}

static void parse_frequency (void)
{
  size_t disp_size = 64 * sizeof(float);
  unsigned int i;
  token = strtok_r(NULL, whitespace, &token_saveptr);
  if (token == NULL) {
    print_filename_linenumber();
    fprintf(stderr, "premature end of line, frequency expected\n");
    exit(EXIT_FAILURE);
  }
  if (strcmp(token, "experiment") != 0) {
    fprintf(stderr, "experiment expected\n");
    exit(EXIT_FAILURE);
  }
  i = 0;
  expfreq = malloc(disp_size);
  assert(expfreq != NULL);
  do {
    token = strtok_r(NULL, whitespace, &token_saveptr);
    if (token == NULL) {
      expfreq[i] = 0.0f;
      break;
    }
    errno = 0;    /* To distinguish success/failure after call */
    tmp_f = strtof(token, &endptr);

    /* Check for various possible errors */
    if (errno != 0 || *endptr != '\0' || !isfinite(tmp_f)) {
      print_filename_linenumber();
      fprintf(stderr, "frequency is invalid value: '%s'\n", token);
      if (errno) {
        perror(NULL);
      }
      exit(EXIT_FAILURE);
    }
    if (tmp_f <= 0.0f) {
      print_filename_linenumber();
      fprintf(stderr, "frequency is non-positive: %f\n", tmp_f);
      exit(EXIT_FAILURE);
    }
    if (i == disp_size - 1) {
      disp_size *= 2;
      expfreq = realloc(expfreq, disp_size);
      assert(expfreq != NULL);
    }
    expfreq[i] = tmp_f;
    ++i;
  } while (1);
}

/**
 * Parse the rest of line (next token) to check for bad input or comment
 */
static void parse_rest_of_line (void)
{
  token = strtok_r(NULL, whitespace, &token_saveptr);
  if (token != NULL && token[0] != '#') {
    print_filename_linenumber();
    fprintf(stderr, "Bad tokens after good tokens: '%s'\n", token);
    exit(EXIT_FAILURE);
  }
}

void initialize (char *argv[])
{
  unsigned int argi;
  truck_capacity = 0.0f;
  service_time = 0u;
  bin_capacity = 0.0f;
  disposal_rate = 0.0f;
  disposal_volume = 0.0f;
  areas_number = 0u;
  stop_time = 0u;
  areas = NULL;

  for (argi = 0u; argv[argi] != NULL; ++argi) {
    filename = argv[argi];
    parse_input_file();
  }


  /* Free line and line copy */
  if (line != NULL) {
    free(line);
    line = NULL;
  }
  if (line_copy != NULL) {
    free(line_copy);
    line_copy = NULL;
  }
  token = NULL;


  check_variables();
}

/**
 * Parse single input script file and initialize global variables from it.
 */
static void parse_input_file (void)
{
  struct area_type *area,       /**< the current area */
                   *areas_end;  /**< the end of the areas */
  static FILE      *file = NULL;  /**< for file reading */

  line_number = 0u;

  file = fopen(filename, "r");
  if (file == NULL) {
    fprintf(stderr, "file %s not found\n", filename);
    exit(EXIT_FAILURE);
  }

  while ((read = getline(&line, &len, file)) != -1) {
    ++line_number;
    if (read == 0 || line[0] == '\n' || line[0] == '#') {
      continue;
    }
    save_line();
    token = strtok_r(line, whitespace, &token_saveptr);
    if (token == NULL) {
      continue;
    }
    if (token[0] == '#') {
      continue;
    }
    if (strcmp(token, "lorryCapacity") == 0) {
      if (truck_capacity != 0u) {
        print_filename_linenumber();
        fputs("warning: override truck capacity\n", stderr);
      }
      truck_capacity = parse_ufloat("truck capacity");

      if (truck_capacity < bin_capacity) {
        print_filename_linenumber();
        fputs("truck capacity is less than the bin capacity\n", stderr);
        exit(EXIT_FAILURE);
      }
    }
    else if (strcmp(token, "serviceTime") == 0) {
      if (service_time != 0u) {
        print_filename_linenumber();
        fputs("warning: override service time\n", stderr);
      }
      service_time = parse_puint("service time");
    }
    else if (strcmp(token, "binCapacity") == 0) {
      if (bin_capacity != 0u) {
        print_filename_linenumber();
        fputs("warning: override bin capacity\n", stderr);
      }
      bin_capacity = parse_ufloat("bin capacity");

      if (bin_capacity < disposal_volume) {
        print_filename_linenumber();
        fputs("bin capacity is less than the disposal volume\n", stderr);
        exit(EXIT_FAILURE);
      }
      if (truck_capacity > 0.0f && truck_capacity < bin_capacity) {
        print_filename_linenumber();
        fputs("truck capacity is less than the bin capacity\n", stderr);
        exit(EXIT_FAILURE);
      }
    }
    else if (strcmp(token, "disposalRate") == 0) {
      if (disposal_rate != 0u) {
        print_filename_linenumber();
        fputs("warning: override disposal rate\n", stderr);
      }
      parse_disposal_rate();
    }
    else if (strcmp(token, "frequency") == 0) {
      parse_frequency();
    }
    else if (strcmp(token, "disposalVol") == 0) {
      if (disposal_volume != 0u) {
        print_filename_linenumber();
        fputs("warning: override disposal volume\n", stderr);
      }
      disposal_volume = parse_ufloat("disposal volume");

      if (bin_capacity > 0.0f && bin_capacity < disposal_volume) {
        print_filename_linenumber();
        fputs("bin capacity is less than the disposal volume\n", stderr);
        exit(EXIT_FAILURE);
      }
    }
    else if (strcmp(token, "noAreas") == 0) {
      if (areas_number != 0u) {
        print_filename_linenumber();
        fputs("warning: override number of areas\n", stderr);
      }
      areas_number = parse_puint("number of areas");
      areas = malloc(areas_number * sizeof(*areas));
      assert(areas != NULL);
      areas_end = areas + areas_number;
      for (area = areas; area != areas_end; ++area) {
        area->frequency = 0.0f;
        area->threshold = 0.0f;
        area->bins_number = 0u;
      }
    }
    else if (strcmp(token, "stopTime") == 0) {
      if (stop_time != 0u) {
        print_filename_linenumber();
        fputs("warning: override stop time\n", stderr);
      }
      /* Cast the time in hours to minutes */
      stop_time = (unsigned int) (60.0f * parse_ufloat("stop time"));
    }
    else if (strcmp(token, "area") == 0) {
      unsigned int  area_i,     /**< index for current area */
                    graph_size, /**< graph size of current area */
                    i,          /**< index for graph rows */
                    j,          /**< index for graph columns */
                  **graph;      /**< points to current area graph */

      area_i = parse_uint("area index");

      if (area_i >= areas_number) {
        print_filename_linenumber();
        fprintf(stderr, "area index %u is more than or equal to the "
                        "number of areas\n", area_i);
        exit(EXIT_FAILURE);
      }
      area = &areas[area_i];
      /* Clean area */
      /* if initialized, bins_number cannot be 0;
       * if not initialized, bins_number are 0 */
      if (area->bins_number != 0u) {
        print_filename_linenumber();
        fprintf(stderr, "warning: override area %u\n", area_i);
        area->frequency = 0.0f;
        area->threshold = 0.0f;
        area->bins_number = 0u;
        free_area(area);
      }
      while ( (token = strtok_r(NULL, whitespace, &token_saveptr)) != NULL )
      {
        if (strcmp(token, "frequency") == 0) {
          if (area->frequency != 0.0f) {
            print_filename_linenumber();
            fputs("warning: override frequency\n", stderr);
          }
          area->frequency = parse_ufloat_area("frequency", area_i);
        }
        else if (strcmp(token, "threshold") == 0) {
          if (area->threshold != 0.0f) {
            print_filename_linenumber();
            fputs("warning: override threshold\n", stderr);
          }
          area->threshold = parse_ufloat_area("threshold", area_i);

          if (area->threshold > 1.0f) {
            print_filename_linenumber();
            fprintf(stderr, "area %u threshold is out of bounds "
                            "(more than 1)\n", area_i);
            exit(EXIT_FAILURE);
          }
        }
        else if (strcmp(token, "bins") == 0) {
          if (area->bins_number != 0u) {
            print_filename_linenumber();
            fputs("warning: override bins number\n", stderr);
          }
          area->bins_number = parse_puint_area("bins", area_i);
        }
        else if (token[0] == '#') {
          break;
        }
        else {
          print_filename_linenumber();
          fprintf(stderr, "unrecognized token: '%s'\n", token);
          exit(EXIT_FAILURE);
        }
      }
      if ((area->frequency == 0.0f && expfreq == NULL) ||
          area->threshold == 0.0f || area->bins_number == 0u) {
        print_filename_linenumber();
        fprintf(stderr, "error in area %u:\n", area_i);
        if (area->frequency == 0.0f && expfreq == NULL) {
          fprintf(stderr, "area %u frequency not initialized\n", area_i);
        }
        if (area->threshold == 0.0f) {
          fprintf(stderr, "area %u threshold not initialized\n", area_i);
        }
        if (area->bins_number == 0u) {
          fprintf(stderr, "area %u bins number not initialized\n", area_i);
        }
        exit(EXIT_FAILURE);
      }
      /* Read next line, increase line number, and stop if line non-empty */
      while ((read = getline(&line, &len, file)) != -1 &&
             (++line_number) &&
             (read == 0 || line[0] == '\n' || line[0] == '#'));
      save_line();
      if (read == -1) {
        print_filename_linenumber();
        fputs("premature end of file\n" "expected graph\n", stderr);
        exit(EXIT_FAILURE);
      }
      token = strtok_r(line, whitespace, &token_saveptr);
      if (token == NULL || strcmp(token, "graph") != 0) {
        print_filename_linenumber();
        fputs("graph expected\n", stderr);
        exit(EXIT_FAILURE);
      }

      /* initialize graph */
      graph_size = area->bins_number + 1;
      area->bins =
          malloc(area->bins_number * sizeof(*area->bins));
      assert(area->bins != NULL);
      area->graph =
          malloc(graph_size * sizeof(*area->graph) +
                 graph_size * graph_size * sizeof(**area->graph));
      assert(area->graph != NULL);
      area->route =
          malloc(graph_size * sizeof(*area->route));
      assert(area->route != NULL);
      area->disposals_data =
          malloc(area->bins_number * sizeof(*area->disposals_data));
      assert(area->disposals_data != NULL);
      area->disposals =
          pqueue_init(area->bins_number, cmp_pri, get_pri, set_pri,
                      get_pos, set_pos);
      assert(area->disposals != NULL);
      graph = area->graph;
      for (i = 0; i < graph_size; ++i) {
        graph[i] =
          (unsigned int*)((char*)graph
              + graph_size * sizeof(*graph)
              + i * graph_size * sizeof(**graph));
      }
      for (i = 0; i < graph_size; ++i) {
        /* Read next line, increase line number, and stop if line non-empty */
        while ((read = getline(&line, &len, file)) != -1 &&
               (++line_number) &&
               (read == 0 || line[0] == '\n' || line[0] == '#'));
        save_line();
        if (read == -1) {
          print_filename_linenumber();
          fprintf(stderr, "premature end of file\n"
                          "expected line %u of graph\n", i);
          exit(EXIT_FAILURE);
        }
        graph[i][0] = parse_uint_graph(line, area_i, i, 0);
        for (j = 1; j < graph_size; ++j) {
          graph[i][j] = parse_uint_graph(NULL, area_i, i, j);
        }
        parse_rest_of_line();
      }
      for (i = 0; i < graph_size; ++i) {
        for (j = i; j < graph_size; ++j) {
          if (graph[i][j] != graph[j][i]) {
            print_filename_linenumber();
            fprintf(stderr, "area %u graph not symmetric\n"
                            "graph[%u][%u] = %u, graph[%u][%u] = %u\n",
                    area_i, i, j, graph[i][j],
                    j, i, graph[j][i]);
            exit(EXIT_FAILURE);
          }
        }
      }
      area = NULL;
      graph = NULL;
    }
    else {
      print_filename_linenumber();
      fputs("unrecognized token\n", stderr);
      exit(EXIT_FAILURE);
    }
    parse_rest_of_line();
  }

  fclose(file);
}

static void check_variables (void)
{
  unsigned int area_i;      /**< index for current area */

  /* Check if global variables are initialized */
  if (truck_capacity == 0.0f || service_time == 0u || bin_capacity == 0.0f ||
      (disposal_rate == 0.0f && expdsprate == NULL) ||
      disposal_volume == 0.0f || areas_number == 0u || stop_time == 0u)
  {
    fputs("error:\n", stderr);
    if (truck_capacity == 0.0f) {
      fputs("truck capacity not initialized\n", stderr);
    }
    if (service_time == 0u) {
      fputs("service time not initialized\n", stderr);
    }
    if (bin_capacity == 0.0f) {
      fputs("bin capacity not initialized\n", stderr);
    }
    if (disposal_rate == 0.0f && expdsprate == NULL) {
      fputs("disposal rate not initialized\n", stderr);
    }
    if (disposal_volume == 0.0f) {
      fputs("disposal volume not initialized\n", stderr);
    }
    if (areas_number == 0u) {
      fputs("number of areas not initialized\n", stderr);
    }
    if (stop_time == 0u) {
      fputs("stop time not initialized\n", stderr);
    }
    exit(EXIT_FAILURE);
  }
  /* Check if areas variables are initialized */
  for (area_i = 0u; area_i < areas_number; ++area_i){
    if ((areas[area_i].frequency == 0.0f && expfreq == NULL) ||
        areas[area_i].threshold == 0.0f || areas[area_i].bins_number == 0u) {
      fprintf(stderr, "error in area %u:\n", area_i);
      if (areas[area_i].frequency == 0.0f && expfreq == NULL) {
        fprintf(stderr, "area %u frequency not initialized\n", area_i);
      }
      if (areas[area_i].threshold == 0.0f) {
        fprintf(stderr, "area %u threshold not initialized\n", area_i);
      }
      if (areas[area_i].bins_number == 0u) {
        fprintf(stderr, "area %u bins number not initialized\n", area_i);
      }
      exit(EXIT_FAILURE);
    }
  }
}

/* Do not warn for unused function - this one is simpler and may be used
 * if the other one is not understood. */
/**
 * Print area variables after they were initialized by {@link #initialize()}.
 *
 * @note This function is intended to be used with print_init_data
 *   As such, its notes apply to this one too - variables must be initialized.
 */
static void print_areas (void)
{
  unsigned int  area_i,         /**< index for current area */
                graph_size,     /**< graph size of current area */
                i,              /**< index for graph rows */
                j,              /**< index for graph columns */
              **graph;          /**< points to current area graph */
  struct area_type *area,       /**< the current area */
                   *areas_end;  /**< the end of the areas */

  areas_end = areas + areas_number;
  area_i = 0;
  for (area = areas; area != areas_end; ++area) {
    printf("area %u:\n" "frequency %f\n" "threshold %f\n"
           "bins_number %u\n" "graph:\n",
           area_i, area->frequency, area->threshold, area->bins_number);
    graph_size = areas[area_i].bins_number + 1;
    graph = area->graph;
    for (i = 0; i < graph_size; ++i) {
      for (j = 0; j < graph_size; ++j) {
        printf("%u\t", graph[i][j]);
      }
      puts("");
    }
    puts("");
    ++area_i;
  }
}

/**
 * Print area variables after they were initialized by {@link #initialize()}.
 * Alternative for {@link #print_areas()}
 *
 * @note This function is intended to be used with print_init_data
 *   As such, its notes apply to this one too - variables must be initilized.
 */
static void print_areas_2 (void)
{
  unsigned int      area_i = 0,     /**< index for current area */
                    graph_size,     /**< graph size of current area */
                    i,              /**< index for graph rows, for newlines */
                   *graph_i,        /**< index for current graph element */
                   *graph_end;      /**< end of current area graph */
  struct area_type *area = areas,   /**< the current area */
                   *areas_end;      /**< the end of the areas */

  areas_end = areas + areas_number;

  do {
    printf("area %u:\n" "frequency %f\n" "threshold %f\n"
           "bins %u\n" "graph:\n",
           area_i++, area->frequency, area->threshold, area->bins_number);
    i = 0;  /* Counter for putting newlines */
    graph_size = area->bins_number + 1;
    graph_i = area->graph[0];
    graph_end = &area->graph[area->bins_number][area->bins_number] + 1;
    do {
      printf("%u\t", *graph_i);
      if (++i == graph_size) {
        i = 0;
        puts("");
      }
    } while (++graph_i != graph_end);
    puts("");
  } while (++area != areas_end);
}

void print_init_data (void)
{
  printf("truck capacity %f\n" "service_time %u\n" "bin_capacity %f\n"
         "disposal_rate %f\n" "disposal_volume %f\n"
         "areas_number %u\n" "stop_time %u\n\n",
         truck_capacity, service_time, bin_capacity, disposal_rate,
         disposal_volume, areas_number, stop_time);
  print_areas_2();
}

void free_data (void)
{
  struct area_type *area = areas,   /**< the current area */
                   *areas_end;      /**< the end of the areas */

  areas_end = areas + areas_number;

  do {
    free_area(area);
  } while (++area != areas_end);
  free(areas);
}

void free_area (struct area_type * const area)
{
  free(area->bins);
  area->bins = NULL;
  free(area->graph);
  area->graph = NULL;
  free(area->route);
  area->route = NULL;
  pqueue_free(area->disposals);
  area->disposals = NULL;
  free(area->disposals_data);
  area->disposals_data = NULL;
  if (expdsprate != NULL) {
    free(expdsprate);
    expdsprate = NULL;
  }
  if (expfreq != NULL) {
    free(expfreq);
    expfreq = NULL;
  }
}
