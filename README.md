Transporter
----

#### Build

#### Standard

```shell
make release
```

#### Verbose log

```shell
make log
```

#### Debug

```shell
make
```

or

```shell
make all
```

#### Extra flags

```shell
make EXTRAFLAGS='-Wno-missing-field-initializers'
```

```shell
make EXTRAFLAGS='-DVERBOSE'
```


#### Clean

```shell
make clean
```

#### Execute

```shell
./a.out [FILE...]
```

### Summary

The input script must be supplied as a command line argument to the program.
It can contain blank lines and comments. When a line starts with `#`, the
rest of the line is ignored.  

Usually you want to build with `make release`. If you want verbose log
information, build with `make log`. If you want to debug the program, build
with `make` or `make all`. If you want to supply extra flags to the compiler,
do this with `EXTRAFLAGS`. You can also get the log when you pass `-DVERBOSE`
to `EXTRAFLAGS`.  

### Generate the documentation

```shell
> doxygen --version
1.8.6
> doxygen Doxyfile-1.8
```

```shell
> doxygen --version
1.6.1
> doxygen Doxyfile-1.6
```

Doxygen 1.6 works fine but Doxygen 1.8 is better and should be preffered.
You can also use the pre-generated 1.8 version in `doc.bz2`.  

### Generate areas

Look at the areas-* files in the root directory. You can test the multithreading
by using the areas.sh script. Supply the number of zones as an argument.

```shell
> dash areas.sh 400 > areas-3 && ./a.out areas-3
```
