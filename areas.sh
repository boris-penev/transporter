#!/bin/sh
i=0
output=''
while [ $i -lt $1 ]
do
  echo 'area '$i' frequency 0.042 threshold 0.7 bins 5
graph
0 8 65535 65535 7 65535
8 0 5 65535 65535 2
65535 5 0 4 65535 6
65535 65535 4 0 2 65535
7 65535 65535 2 0 3
65535 2 6 65535 3 0
'
  i=$(( $i + 1 ))
done
