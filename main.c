/**
 * @file
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include "pqueue.h"
#include "main.h"
#include "init.h"
#include "sim.h"


int main (int argc, char *argv[])
{
  /* Check if input file is given as a command line argument.
   * We could test for zero pointers in argv instead of argc. */
  if (argc < 2) {
    fputs("no command line arguments given - expected filename\n", stderr);
    exit(EXIT_FAILURE);
  }

  /* Pass command line arguments to initialize, except the first element,
   * which should be the name of the executable. */
  initialize(&argv[1]);

  simulate();
  free_data();

  return 0;
}
