/**
 * @file
 */

#pragma once

#include <stdint.h>

#include "pqueue.h"

/* In C89 inline keyword is not supported so GCC extension is used */
#if (!defined (__STDC_VERSION__) || __STDC_VERSION__ < 199901L) \
  && defined (__GNUC__)
#define inline __inline__
#endif

/* Define global variables that will be used in the whole program. */

/** data structure for priority queue (heap) nodes */
struct node {
  pqueue_pri_t pri;             /**< priority of the node */
  size_t       pos;             /**< position of the node in the queue */
  unsigned int index;           /**< index of the node, for example
                                 *   bin index or graph node index */
};

/** data structure for route nodes */
struct route_node {
  unsigned int i;               /**< index of the node, for example */
  unsigned int time;            /**< time of the route */
};

/** data structure for bins */
struct bin {
  float occupancy;             /**< current occupancy of the bin */
  unsigned int overflown;      /**< whether the bin has ever overflown */
};

/** data structure for areas */
struct area_type {
  float             frequency;      /**< bin collection frequency */
  float             threshold;      /**< current occupancy of the bin */
  unsigned int      bins_number;    /**< number of bins */
  float             truck_occupancy;/**< current occupancy of the truck */
  uint64_t          trips_tmr;      /**< timer of all trips */
  unsigned int      crr_trp;        /**< time of current trip start */
  unsigned int      trips_n;        /**< number of trips */
  double            grbgcol;        /**< total garbage collected */
  unsigned int      trpint;         /**< trip interrupted */
  unsigned int      schdl;          /**< scheduled trip */
  unsigned int      ovrfl;          /**< overflowed bins */
  struct bin      * bins;           /**< indiviual bin information */
  unsigned int   ** graph;          /**< graph representation of the area
                                     *   as adjacency matrix */
  struct route_node *route;         /**< service route */
  pqueue_t        * disposals;      /**< priority queue for bin disposals */
  struct node     * disposals_data; /**< data for bin disposals */
} *areas;

float         truck_capacity;     /**< capacity of the trucks */
unsigned int  service_time;       /**< time length of bin service */
float         bin_capacity;       /**< capacity of the bins */
float         disposal_rate;      /**< rate of garbage disposal */
float         disposal_volume;    /**< volume of garbage disposal */
unsigned int  areas_number;       /**< number of areas */
unsigned int  stop_time;          /**< ending point of simulation, in minutes */
float       * expfreq;            /**< experimental bin collection frequency */
float       * expdsprate;         /**< experimental disposal rate */


/* Functions for the priority queue */

static inline int cmp_pri(pqueue_pri_t next, pqueue_pri_t curr)
{
  return (next > curr);
}

static inline pqueue_pri_t get_pri(void *a)
{
  return ((struct node *) a)->pri;
}

static inline void set_pri(void *a, pqueue_pri_t pri)
{
  ((struct node *) a)->pri = pri;
}

static inline size_t get_pos(void *a)
{
  return ((struct node *) a)->pos;
}

static inline void set_pos(void *a, size_t pos)
{
  ((struct node *) a)->pos = pos;
}
