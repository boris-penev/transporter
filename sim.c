/**
 * @file
 */
#define _SVID_SOURCE
#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <limits.h>
#include <assert.h>
#include <math.h>
#include <time.h>

#include "main.h"
#include "sim.h"

#define RAND_STATELEN 64

static struct random_data sh_rand_buf;
static char sh_rand_statebuf[RAND_STATELEN];

static void * simulate_area (void * const arg);
static void  initialize_area (struct area_type   * const area,
                              struct random_data * const rand_buf);
static unsigned int sim_random (struct random_data * const rand_buf,
                                const float freq);
static void dijkstra   (unsigned int ** const graph,
                        const unsigned int    gsize,
                        unsigned int * const  dist,
                        unsigned int * const  scnd,
                        pqueue_t     * const  q,
                        struct node  * const  ds);
static void  dispose   (struct area_type    * const area,
                        struct node         * const d,
                        struct random_data  * const rand_buf);
static void  sch_route (struct area_type    * const area,
                        struct random_data  * const rand_buf);
static struct route_node *
            service    (struct area_type    * const area,
                        struct route_node   *       r,
                        struct random_data  * const rand_buf);
static void  gen_route (struct area_type    * const area,
                        unsigned int        * const vst,
                        struct random_data  * const rand_buf);
extern int          cmp_pri (pqueue_pri_t next, pqueue_pri_t curr);
extern pqueue_pri_t get_pri (void *a);
extern void         set_pri (void *a, pqueue_pri_t pri);
extern size_t       get_pos (void *a);
extern void         set_pos (void *a, size_t pos);

void simulate (void)
{
  pthread_t    *threads,        /**< begin of threads */
               *threads_end,    /**< end of threads */
               *thrd;           /**< current thread (iterator) */
  struct area_type *area = areas,  /**< current area (iterator) */
            * const areas_end =    /**< end of areas */
                            areas + areas_number;
  uint64_t      stripst = 0UL,  /**< sum of trip times */
                stripsn = 0UL,  /**< sum of the number of trips */
                ssched  = 0UL,  /**< sum of the number of schedules */
                sbins   = 0UL,  /**< sum of the bins */
                soverf  = 0UL;  /**< sum of the overflown bins */
  double        sgarcol = 0.0;  /**< sum of the total garbage collected */
  float       * dspr_i,         /**< disposal rate iterator */
              * freq_i;         /**< disposal rate iterator */

  initstate_r((unsigned int)time(NULL), sh_rand_statebuf, RAND_STATELEN,
              &sh_rand_buf);

  threads = calloc(areas_number, sizeof(pthread_t));
  assert(threads != NULL);
  threads_end = threads + areas_number;

  dspr_i = expdsprate;
  while (1) {
    if (expdsprate != NULL) {
      if (*dspr_i == 0.0f) {
        break;
      }
      disposal_rate = *dspr_i;
      printf("\ndisposal rate experiment %0.2f\n\n", disposal_rate);
    }
    freq_i = expfreq;
    while (1) {
      if (expfreq != NULL) {
        if (*freq_i == 0.0f) {
          break;
        }
        area = areas;
        do {
          area->frequency = *freq_i;
        } while (++area != areas_end);
        printf("\nfrequency experiment %0.4f\n\n", *freq_i);
      }
      stripst = 0UL;
      stripsn = 0UL;
      ssched  = 0UL;
      sbins   = 0UL;
      soverf  = 0UL;
      sgarcol = 0.0;
      area = areas;
      thrd = threads;
      do {
        assert(area != areas_end);
        pthread_create(thrd, NULL, simulate_area, area);
        ++area;
      } while (++thrd != threads_end);

      thrd = threads;
      do {
        pthread_join(*thrd, NULL);
      } while (++thrd != threads_end);

      area = areas;
      do {
        stripst += area->trips_tmr;
        stripsn += area->trips_n;
        sgarcol += area->grbgcol;
        ssched  += area->schdl;
        sbins   += area->bins_number;
        soverf  += area->ovrfl;
      } while (++area != areas_end);

      puts("");
      printf("average trip duration %.2f\n",
              stripsn ? (double)stripst / (double)stripsn / 60.0 : 0.0);
      printf("trip efficiency %.5f\n",
              stripst ?    sgarcol      / (double)stripst / 60.0 : 0.0);
      printf("average trips %.2f\n",
              ssched  ? (double)stripst / (double)ssched : 0.0);
      printf("bins overflowed percentage %.2f\n",
              (double)soverf / (double)sbins * 100.0);
      if (expfreq != NULL) {
        ++freq_i;
      } else {
        break;
      }
    }
    if (expdsprate != NULL) {
      ++dspr_i;
    } else {
      break;
    }
  }
  free(threads);

}

/**
 * Perform simulation on one area
 * @param arg const pointer to the area (the pointer will not be changed)
 */
static void * simulate_area (void * const arg)
{
  struct area_type * const area = arg;
  struct random_data rand_buf = { 0 };
  char rand_statebuf[RAND_STATELEN] = { 0 };
  int32_t seed;
  struct node       * d;        /**< nodes for dispose, route */
  struct route_node * r =       /**< nodes for dispose, route */
                        area->route;
  unsigned int  * const vst =   /**< visited bins in gen_route */
                            malloc(area->bins_number * sizeof(unsigned int));
  struct bin     *b   = area->bins, /*< bin iterator */
          * const bins_end =        /**< end of bin disposals */
                        area->bins + area->bins_number;

  random_r(&sh_rand_buf, &seed);
  initstate_r((unsigned int)seed, rand_statebuf, RAND_STATELEN, &rand_buf);

  initialize_area(area, &rand_buf);

  do {
    d = pqueue_peek(area->disposals);
    if ((r->time < d->pri ? r->time : d->pri) >= stop_time) {
      break;                /* If minimal time is more than stop time, break */
    }
    if (r->time < d->pri) {
      if (r->i == UINT_MAX) {
        gen_route(area, vst, &rand_buf);
        r = area->route;
      } else {
        r = service(area, r, &rand_buf);
      }
    } else {
      dispose(area, d, &rand_buf);
    }
  } while (1);

  free(vst);
  while(pqueue_size(area->disposals) != 0) {
    pqueue_pop(area->disposals);
  }
  memset(area->disposals_data, 0u, area->bins_number * sizeof(*area->disposals_data));

  do {
    if (b->overflown) {
      ++area->ovrfl;
    }
  } while (++b != bins_end);

  printf("average trip duration area %lu %.3f\n", area - areas,
         area->trips_n ?
                (double)area->trips_tmr / (double)area->trips_n / 60.0 : 0.0);
  printf("trip efficiency area %lu %.5f\n", area - areas,
         area->trips_tmr ?
                        area->grbgcol / (double)area->trips_tmr / 60.0 : 0.0);
  printf("average trips area %lu %.2f\n", area - areas,
         area->schdl ? (double)area->trips_tmr / (double)area->schdl : 0.0);
  printf("bins overflowed percentage area %lu %.2f\n", area - areas,
         (double)area->ovrfl / (double)area->bins_number * 100.0);

  /* pthread_exit(NULL); */
  return NULL;
}

/**
 * Initialize the area
 * @param area      const pointer to the area
 * @param rand_buf  const pointer to the PRNG buffer
 */
static void  initialize_area (struct area_type   * const area,
                              struct random_data * const rand_buf)
{
  struct node    *dsp =        /**< current disposal node */
                        area->disposals_data,
          * const disposals_end =   /**< end of bin disposals */
                        area->disposals_data + area->bins_number;
  struct bin     *bin = area->bins, /*< current bin */
          * const bins_end =        /**< end of bin disposals */
                        area->bins + area->bins_number;
  unsigned int    i = 0u,           /**< current bin/node index */
                  j;
  const unsigned int    gsize =     /**< graph size */
                          area->bins_number + 1;
  unsigned int        * dist,       /**< distances */
                * const scnd =      /**< scanned */
                        malloc(gsize * sizeof(unsigned int));
  struct node   * const ds =        /**< data for the queue */
                        malloc(gsize * sizeof(struct node));
  pqueue_t      * const q =         /**< priority queue for distances */
                        pqueue_init(gsize, cmp_pri, get_pri,
                                    set_pri, get_pos, set_pos);

  assert(ds != NULL);
  assert(q != NULL);
  assert(scnd != NULL);

  area->truck_occupancy = 0.0f;
  area->trips_tmr = 0UL;
  area->crr_trp = 0u;
  area->trips_n = 0u;
  area->grbgcol = 0.0;
  area->trpint  = 0u;
  area->schdl   = 0u;
  area->ovrfl   = 0u;

  /* Zero fill bins */
  do {
    bin->occupancy = 0.0f;
    bin->overflown = 0u;
  } while (++bin != bins_end);

  /* Generate disposal events and fill them in the queue */
  do {
    dsp->index = i++;
    dsp->pri = sim_random(rand_buf, disposal_rate);
    pqueue_insert(area->disposals, dsp);
  } while (++dsp != disposals_end);

  /* Schedule the next service trip start time */
  area->route->time = 0u;
  sch_route(area, rand_buf);

  /* Run Dijkstra algorithm for every vertex and build a complete graph */
  for (i = 0u; i < gsize; ++i) {
    dist = area->graph[i];      /* We build a complete graph using dist */
    dijkstra(area->graph, gsize, dist, scnd, q, ds);
    for (j = 0u; j < gsize; ++j) {  /* The graph is undirected */
      area->graph[j][i] = dist[j];  /* We make it symmetric */
    }
  }

  free(scnd);
  pqueue_free(q);
  free(ds);
}

/**
 * Return random number
 * @param rand_buf  const pointer to the PRNG buffer
 * @param freq      frequency of the event
 * @return          random number
 */
static unsigned int sim_random (struct random_data * const rand_buf,
                                const float freq)
{
  int32_t rand_res;             /**< Result of the random_r */
  double  prob;                 /**< Probability */

  /* (unsigned int) (x + 0.5) is like round(x) */
  random_r(rand_buf, &rand_res);
  prob = (double)rand_res / (double)RAND_MAX;
  return (unsigned int) (((log(prob)*(-60.0))/(double)freq) + 0.5);
}

/**
 * Generate shortest distances between source vertex and all the others
 * The comments are from wikipedia
 * @param graph  graph to search
 * @param gsize  graph size
 * @param dist   distances array
 * @param scnd   scanned array
 * @param q      priority queue for distances
 * @param ds     data for the queue
 */
static void dijkstra   (unsigned int ** const graph,
                        const unsigned int    gsize,
                        unsigned int * const  dist,
                        unsigned int * const  scnd,
                        pqueue_t     * const  q,
                        struct node  * const  ds)
{
  unsigned int v, u, alt;
  struct node             * d = ds;   /**< single node */
  const struct node * const ds_end =  /**< end of data for the queue */
                                  ds + gsize;

  memset(scnd, 0u, gsize * sizeof(unsigned int));

  for (v = 0; v < gsize; ++v) { /* Initializations */
    d->index = v;
    d->pri = dist[v];
    pqueue_insert(q, d);        /* All nodes initially in q (unvisited nodes) */
    ++d;
  }

  while (pqueue_size(q) != 0) {   /* The main loop */
    d = pqueue_pop(q);            /* Source node in first case, because its
                                   * distance to itself is 0, the minimal */
    u = d->index;                 /* Remove and return best vertex */
    scnd[u] = 1;
    for (v = 0; v < gsize; ++v) { /* For each neighbor v of u */
      if (graph[u][v] == USHRT_MAX || v == u || scnd[v] != 0) {
        continue;                 /* Not neighbor or scanned */
      }
      alt = dist[u] + graph[u][v];/* Alternative path */
      if (alt < dist[v]) {        /* A shorter path to v has been found */
        dist[v] = alt;
        d = &ds[v];
        assert(d != ds_end);
        pqueue_change_priority(q, alt, d);  /* decrease priority */
      }
    }
  }
}

/**
 * Dispose a bin
 * @param area      const pointer to the area
 * @param d         the top disposal node
 * @param rand_buf  const pointer to the PRNG buffer
 */
static void  dispose   (struct area_type    * const area,
                        struct node         * const d,
                        struct random_data  * const rand_buf)
{
  struct bin * const b = &area->bins[d->index];  /**< current bin */

#ifdef VERBOSE
printf("bag disposed at bin %lu.%u at time %.2f\n",
       area - areas, d->index + 1, d->pri/60.0);
if (!(b->occupancy + disposal_volume < area->threshold * bin_capacity)) {
printf("bin %lu.%u occupancy threshold exceeded at time %.2f\n",
       area - areas, d->index + 1, d->pri/60.0);
}
if (b->occupancy + disposal_volume > bin_capacity) {
printf("bin %lu.%u overflowed at time %.2f\n",
       area - areas, d->index + 1, d->pri/60.0);
}
#endif

  if (b->occupancy + disposal_volume > bin_capacity) {
    b->occupancy = bin_capacity;
    b->overflown = 1;
  } else {
    b->occupancy = b->occupancy + disposal_volume;
  }
  pqueue_change_priority(area->disposals,
                         d->pri + sim_random(rand_buf, disposal_rate), d);
}

/**
 * Schedule start time for the next trip
 * @param area      const pointer to the area
 * @param rand_buf  const pointer to the PRNG buffer
 * @note We set the index of the route node to UINT_MAX to indicate that this
 *    is not part of a normal route but just a schedule for the next one.
 */
static void  sch_route (struct area_type    * const area,
                        struct random_data  * const rand_buf)
{
  area->route->i = UINT_MAX;
  area->route->time += sim_random(rand_buf, area->frequency);
  ++area->schdl;
}

/**
 * Service a bin
 * @param area      const pointer to the area
 * @param r         the top route node
 * @param rand_buf  const pointer to the PRNG buffer
 * @return          the next node
 */
static struct route_node *
            service    (struct area_type    * const area,
                        struct route_node   *       r,
                        struct random_data  * const rand_buf)
{
  struct bin        * b;

#ifdef VERBOSE
printf("lorry %lu arrives at location %lu.%u at time %.2f\n",
       area - areas, area - areas, r->i, r->time/60.0);
#endif
  /* We are in the depot */
  if (r->i == 0u) {
    area->trips_tmr += r->time - area->crr_trp;
    ++area->trips_n;
    area->grbgcol += area->truck_occupancy;
    area->truck_occupancy = 0.0f;
    area->route->time = r->time;
    if (area->trpint) {
      area->trpint = 0;
      area->route->i = UINT_MAX;  /* Emergency replanning */
    } else {
      sch_route(area, rand_buf);  /* Reschedule */
    }
    return area->route;
  }

  /* We are to a bin */
  b = &area->bins[r->i - 1];

#ifdef VERBOSE
printf("bin %lu.%u emptied at time %.2f\n",
       area - areas, r->i, (r->time + service_time)/60.0);
#endif

  /* Service the bin */
  if (area->truck_occupancy + b->occupancy < truck_capacity) {
    area->truck_occupancy += b->occupancy;
    b->occupancy = 0.0f;
  } else {
    b->occupancy -= truck_capacity - area->truck_occupancy;
    area->truck_occupancy = truck_capacity;
  }

#ifdef VERBOSE
printf("lorry %lu leaves location %lu.%u at time %.2f\n",
       area - areas, area - areas, r->i, (r->time + service_time)/60.0);
#endif

  /* Redirect to the depot if the next bin cannot be serviced.
   * We presume that the next bin is occupied exactly to the threshold. */
  if (truck_capacity - area->truck_occupancy <
      area->threshold * bin_capacity) {
    area->trpint = 1;
    area->route->time = r->time + area->graph[r->i][0] + service_time;
    area->route->i = 0;
    return area->route;
  }

  return ++r;                     /* Return next node */
}

/**
 * Generate the route
 * @param area  const pointer to the area
 * @param vst   visited bins
 * @param rand_buf  const pointer to the PRNG buffer
 * @note The valid route, it ends with node with index 0
 */
static void  gen_route (struct area_type    * const area,
                        unsigned int        * const vst,
                        struct random_data  * const rand_buf)
{
  const float           thrsh =       /* threshold capacity */
                            area->threshold * bin_capacity;
  unsigned int          i,          /**< current root index */
                        u,          /**< current vertex index */
                        j,          /**< neighbor vertex index */
                        mini =      /**< minimal path root vertex excl. depot */
                            UINT_MAX,
                        mind =      /**< minimal path distance */
                            UINT_MAX,
                        nni,        /**< nearest neighbor index */
                        nnd,        /**< nearest neighbor distance */
                        tplen = 0u, /**< target path length (vertices) */
                        cplen,      /**< current path length (vertices) */
                        cpd,        /**< current path distance */
                        fill_route=0u;/**< flag whether the best root vertex was
                                      *    found and we should fill the route */
  struct route_node   * r =         /**< route iterator */
                            area->route;
  unsigned int          ctime =     /**< current time */
                            r->time;

  for (i = 0u; i < area->bins_number; ++i) {     /* For every bin */
    if (!(area->bins[i].occupancy < thrsh)) {   /* over the threshold */
      ++tplen;
    }
  }

  for (i = 0; i < area->bins_number; ++i) {     /* For every bin */
    if (area->bins[i].occupancy < thrsh) {      /* over the threshold */
      continue;
    }
    memset(vst, 0u, area->bins_number * sizeof(unsigned int));
    u = i;
    vst[u] = 1;                                 /* Mark as visited */
    cplen = 1;                                  /* Initialize current path */
    cpd = area->graph[0][u+1];
    if (fill_route) {
      assert(r != area->route + area->bins_number + 1);
      r->i = u + 1;
      ctime += area->graph[0][u+1];
      r->time = ctime;
      ++r;
    }
    while (cplen < tplen) {
      nni = UINT_MAX;
      nnd = UINT_MAX;
      for (j = 0; j < area->bins_number; ++j) {  /* For every other bin */
        if (area->bins[j].occupancy < thrsh || vst[j] != 0u) {
          continue;                 /* Ensure not visited and over threshold */
        }
        if (area->graph[u+1][j+1] < nnd) {
          nni = j;
          nnd = area->graph[u+1][j+1];
        }
      }
      u = nni;
      vst[u] = 1;                                 /* Mark as visited */
      ++cplen;
      cpd += nnd;
      if (fill_route) {
        assert(r != area->route + area->bins_number + 1);
        r->i = u + 1;
        ctime += service_time + nnd;    /* Add previous bin service time */
        r->time = ctime;
        ++r;
      }
    }
    if (fill_route) {
      break;
    }
    if (cpd < mind) {
      mini = i;
      mind = cpd;
    }
    if (i == area->bins_number - 1) {
      fill_route  = 1;
      i = mini;
    }
  }

  /* If no bins should be visited, reschedule */
  if (fill_route == 0) {
    sch_route(area, rand_buf);
    return;
  }

  /* There is a valid route */

  r->i = 0u;                      /* Add the depot node */
  ctime += service_time + area->graph[u + 1][0];
  r->time = ctime;

  area->crr_trp = area->route->time;

#ifdef VERBOSE
printf("lorry %lu leaves location %lu.%u at time %.2f\n",
       area - areas, area - areas, area->route->i, area->route->time/60.0);
#endif
}
