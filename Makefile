CC = gcc
CFLAGS = -std=c89 -Wall -Wextra -pedantic -Wconversion -Winline -ggdb3
LDFLAGS = -lm -pthread
SOURCES = pqueue.c main.c init.c sim.c

# Set EXTRAFLAGS if you want additional compiler flags
# For example, EXTRAFLAGS=-w

# default goal is by convention all
all: $(SOURCES)
	$(CC) $(CFLAGS) $(EXTRAFLAGS) $(SOURCES) $(LDFLAGS)

release: CC = gcc
release: CFLAGS = -O3 -march=native -DNDEBUG
release: all
.PHONY: release

log: CC = gcc
log: CFLAGS = -O3 -march=native -DNDEBUG -DVERBOSE
log: all
.PHONY: log

.PHONY: clean
clean:
	rm -f a.out
