/**
 * @file
 */

#pragma once

#include "main.h"

/**
 * Parse input script files and initialize global variables.
 * @param argv array with the filenames of the script files
 */
void initialize (char *argv[]);

/**
 * Print global variables after they were initialized by {@link #initialize()}.
 *
 * @note Do not call this function before the global variables are initialized
 * by the {@link #initialize()} function. Otherwise, its behaviour is undefined.
 */
void print_init_data (void);

/**
 * Free global variables and areas.
 */
void free_data (void);

/**
 * Free area
 * @param area const pointer to the area
 */
void free_area (struct area_type * const area);
