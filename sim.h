/**
 * @file
 */

#pragma once

#include "main.h"

/**
 * Perform simulation
 */
void simulate (void);
